var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var booksSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    pages: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    created: { 
        type: Date 
    }, 
    updated: {
      type: Date 
    }
});
var Books = mongoose.model('Books',booksSchema);

module.exports = Books;