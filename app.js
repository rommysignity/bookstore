process.env.NODE_CONFIG_DIR = __dirname + '/config/';
var config = require('config');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var mongoose = require('./database/index');
var fileupload = require('express-fileupload');
var index = require('./routes/index');
var users = require('./routes/users');
var books = require('./routes/books');

var app = express();
app.set('port', process.env.PORT || config.get('port'));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
global.appRoot = path.resolve(__dirname);
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use('/static',express.static(path.join(__dirname, 'public')));
app.use(fileupload());
app.use('/', index);
app.use('/books',books);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
http.createServer(app).listen(app.get('port'), function () {
  var db = mongoose.connection;
  db.on('error', console.error.bind(console,'connection error: '));
  db.once('open', function(){
    console.log('***********************************************');
    console.log("*     Connected to database!                  *");
    console.log('*     server listening on port ' + app.get('port')+' *');
    console.log('***********************************************');

  });


});
module.exports = app;
