var express = require('express');
var router = express.Router();
var Books = require('../models/books');
/* GET home page. */
router.get('/', function (req, res, next) {
    Books.find({}, function (err, books) {
        if (err) {
            throw err;
        }
        res.render('index', {title: 'Books Store', books: books});
    }).sort([['created', 'descending']]).limit(4);
});

module.exports = router;
