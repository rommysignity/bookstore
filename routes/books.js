var express = require('express');
var path = require("path");
var router = express.Router();
var randomString = require('randomstring');
var Books = require('../models/books');
router.get('/:page(\\d+)?', function (req, res, next) {
    var skip = 0;
    var limit = 2;
    if (typeof req.params.page !== 'undefined') {
        skip = (parseInt(req.params.page) - 1) * limit;
    }

    Books.count({}, function (err, count) {
        Books.find({}, function (err, books) {
            if (err) {
                throw err;
            }
            var numofPages = 0;
            if(count) {
                if(count > limit) {
                    numofPages = Math.ceil(count/limit);
                } else {
                    numofPages = 1;
                }
            }
            res.render('books/list', {title: 'List | Books Store', books: books,numofPages: numofPages});
        }).skip(skip).limit(limit);
    });
});

router.get('/add', function (req, res, next) {
    res.render('books/add', {title: 'Add New Book'});
})
.post('/add', function (req, res, next) {
    var sampleFile = req.files.image;
    var allowed = -1;
    if(typeof sampleFile !== 'undefined') {
        var tempName = sampleFile.name;
        var allowedExt = ['jpeg','png','jpg','gif'];
        var ext = tempName.substr(tempName.lastIndexOf('.') + 1);
        allowed = allowedExt.indexOf(ext);
    }
    var newName = randomString.generate({
        length: 12,
        charset: 'alphabetic'
    });
    var data = req.body;
    data.image = '';
    if(allowed !== -1) {
       data.image =  newName + '.' +ext;
    }
    var dataToSave = Books(data);
    dataToSave.created = new Date();
    dataToSave.updated = new Date();
    dataToSave.save(function (err) {
        if (err) {
            data.title = 'Edited';
            res.render('books/add', data);
        } else {
            sampleFile.mv(appRoot+'/public/uploads/'+ newName +'.'+ext,function(err) {
                if(err) {
                    throw err;
                }
                res.redirect('/');
            });
        }
    });
});

router.get('/book/:id', function (req, res, next) {
    Books.findById(req.params.id, function (err, book) {
        if (err) {
            throw err;
        }
        if (book) {
            var data = book;
            data.title = book.name + ' | Book Store';
            res.render('books/view', data);
        } else {
            res.redirect('/');
        }
    });
});
router.get('/delete/:id', function (req, res, next) {
    Books.findById(req.params.id, function (err, book) {
        if (err) {
            throw err;
        }
        book.remove(function (err) {
            if (err) {
                throw err;
            }
            res.redirect('/books');
        });
    });
});

router.get('/edit/:id', function (req, res, next) {
    Books.findById(req.params.id, function (err, book) {
        if (err) {
            throw err;
        }
        var data = book;
        data.title = book.name + ' | Book Store';
        res.render('books/edit', data);
    });
})
.post('/edit/:id', function (req, res, next) {
    Books.findById(req.params.id, function (err, book) {
        if (err) {
            throw err;
        }
        book.name = req.body.name;
        book.author = req.body.author;
        book.description = req.body.description;
        book.price = req.body.price;
        book.pages = req.body.pages;
        book.note = req.body.note;
        book.updated = new Date();
        book.save(function (err) {
            if (err) {
                var data = book;
                data.title = book.name + ' | Book Store';
                res.render('books/edit', data);
            } else {
                res.redirect('/books/book/'+req.params.id);
            }
        });
    });
});

module.exports = router;